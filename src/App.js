import React from 'react';
import { Game } from 'containers'

function App() {
  return (
    <div className="App">
      <Game />
    </div>
  );
}

export default App;
