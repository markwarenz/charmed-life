import React from 'react';
import { Button } from '@material-ui/core';
import PropTypes from 'prop-types';
import { personalityQuizQuestions } from 'data';
import { shuffleArray } from "helpers";

const PersonalityQuiz = ({ handleQuizResult }) => {
    // Myers Briggs personality quiz, just a few questions
    // results in 4 character personality type
    const [questions, setQuestions] = React.useState([]);
    const [currentQuestion, setCurrentQuestion] = React.useState(0);
    const [score, setScore] = React.useState({
        i: 0,
        e: 0,
        s: 0,
        n: 0,
        t: 0,
        f: 0,
        j: 0,
        p: 0,
    });
    const calcResult = () => {
        const ie = (score.i > score.e) ? 'I' : 'E';
        const sn = (score.s > score.n) ? 'S' : 'N';
        const tf = (score.t > score.f) ? 'T' : 'F';
        const jp = (score.j > score.p) ? 'J' : 'P';
        return `${ie}${sn}${tf}${jp}`;
    }
    const isComplete = (currentQuestion === questions.length);
    React.useEffect(() => {
        console.log('shuffling Array');
        setQuestions(shuffleArray(personalityQuizQuestions));
    }, []);
    const handleOptionClick = (v) => {
        const newScore = score[v] + 1;
        setScore({
            ...score,
            [v]: newScore,
        });
        if (isComplete){
            // process complete. Calc Score & return
        } else {
            setCurrentQuestion(currentQuestion + 1);
        }
    }
    const shuffledOptions = (isComplete) ? [] : questions[currentQuestion].qs;
    console.log(questions);
    return (
        <div>
            I: {score.i},
            E: {score.e},
            S: {score.s},
            N: {score.n},
            T: {score.t},
            F: {score.f},
            J: {score.j},
            P: {score.p},
            {
                isComplete ?
                    <div>
                        You are complete. Here is your result: {calcResult()}
                    </div>
                    : <div>
                        <h2>{questions[currentQuestion].label}</h2>
                        {
                            shuffledOptions.map((opt, idx) => <Button key={idx} onClick={() => handleOptionClick(opt.v)}>
                                {opt.q}
                            </Button>)
                        }
                    </div>
            }
            {(currentQuestion + 1)}
        </div>
    )
};

PersonalityQuiz.propTypes = {
    handleQuizResult: PropTypes.func.isRequired,
};

export default PersonalityQuiz;
