const personalityQuizQuestions = [
    // I vs E
    {
        label: "When it comes to social gatherings...",
        qs: [
            {
                q: "I prefer the company of a small group of friends or time alone.",
                v: "i"
            },
            {
                q: "Concerts. Parties. Shopping on Black Friday. I love crowds.",
                v: "e"
            }
        ],
    },
    {
      label: "Given a set of tasks to do...",
        qs: [
            {
                q: "I like to do one thing at a time.",
                v: "i"
            },
            {
                q: "I'm a multi-tasker. I switch between tasks easily.",
                v: "e"
            }
        ],
    },
    {
        label: "When meeting new people...",
        qs: [
            {
                q: "I sometimes feel anxious.",
                v: "i"
            },
            {
                q: "I love it. I get charge out of being around people.",
                v: "e"
            }
        ],
    },


    // S vs N
    {
        label: "In general...",
        qs: [
            {
                q: "I like to focus on the tiny details.",
                v: "s"
            },
            {
                q: "I like to think about the big picture.",
                v: "n"
            }
        ],
    },
    {
        label: "When solving a problem...",
        qs: [
            {
                q: "I lean on past experience and common sense.",
                v: "s"
            },
            {
                q: "I rely on intuitiion and creativity.",
                v: "n"
            }
        ],
    },
    {
        label: "When making decisions...",
        qs: [
            {
                q: "I look for facts.",
                v: "s"
            },
            {
                q: "I look for patterns.",
                v: "n"
            }
        ],
    },


    // T vs F
    {
        label: "I tend to make decisions using...",
        qs: [
            {
                q: "logical analysis.",
                v: "t"
            },
            {
                q: "my personal beliefs and values.",
                v: "f"
            }
        ],
    },
    {
        label: "With big choices, my favorite tool is...",
        qs: [
            {
                q: "a list of pros and cons.",
                v: "t"
            },
            {
                q: "feedback from other people.",
                v: "f"
            }
        ],
    },
    {
        label: "In general, I'd consider myself...",
        qs: [
            {
                q: "a thinker",
                v: "t"
            },
            {
                q: "a feeler",
                v: "f"
            }
        ],
    },

    // J vs P
    {
        label: "When making plans, it's most important to...",
        qs: [
            {
                q: "stick with them.",
                v: "j"
            },
            {
                q: "stay flexible and keep options open.",
                v: "p"
            }
        ],
    },
    {
        label: "It's most important to me to be...",
        qs: [
            {
                q: "neat and organized.",
                v: "j"
            },
            {
                q: "flexible and spontenous.",
                v: "p"
            }
        ],
    },
    {
        label: "When taking time off...",
        qs: [
            {
                q: "I like to have something concrete planned for every day.",
                v: "j"
            },
            {
                q: "I like to play it by ear.",
                v: "p"
            }
        ],
    },


];
export default personalityQuizQuestions;
