import initVals from './initVals';
import personalityQuizQuestions from "./personalityQuizQuestions";
export {
    initVals,
    personalityQuizQuestions,
}
