const initVals = {
    phase: '',
    personality: '',
    flags: [],
    debt: 0,
    possessions: [],
    attributes: {
        age: 14,
        health: 100,
        ethics: 100,
        wealth: 0,
        socialCapital: 20,
        notoriety: 0,
    }
};

export default initVals;
