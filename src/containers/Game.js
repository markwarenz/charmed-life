import React from 'react';
import { initVals } from 'data';
import { Button } from '@material-ui/core';
import { PersonalityQuiz } from 'components';

const Game = () => {
    const [player, setPlayer] = React.useState(initVals);
    const restartGame = () => {
        setPlayer(initVals);
    };
    const handleClick = () => {
        let tmpPlayer = player;
        tmpPlayer = {
            ...tmpPlayer,
            x: 'one',
        };
        tmpPlayer = {
            ...tmpPlayer,
            y: 'two',
        };
        setPlayer(tmpPlayer);
    };
    const handleQuizResult = (result) => {
        let tmpPlayer = player;
        tmpPlayer = {
            ...tmpPlayer,
            personality: result,
        }
        setPlayer(tmpPlayer);
    };
    console.log('player', player);
    return (
        <div>

            <PersonalityQuiz
                handleQuizResult={handleQuizResult}
            />


            <div style={{ display: 'none' }}>
                Yo yo game.
                <Button onClick={handleClick}>
                    OK
                </Button>


                <Button onClick={restartGame}>
                    Quit
                </Button>
            </div>

        </div>
    )
};

export default Game;
